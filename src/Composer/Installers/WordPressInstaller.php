<?php
namespace Composer\Installers;

class WordPressInstaller extends BaseInstaller
{
    protected $locations = array(
        'muplugin' => './web/mu-plugins/{$name}/',
        'plugin' => './web/plugins/{$name}/',
        'theme' => './web/themes/{$name}/',
        'source' => './web/wp/',
        'core' => './web/wp',
    );
}
